package com.wavelabs.model.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.builder.ObjectBuilder;
import com.wavelabs.model.Message;

@RunWith(MockitoJUnitRunner.class)
public class MessageTest {

	@InjectMocks
	private Message mockMessage;

	private Message message;

	@Before
	public void setUp() {

		message = ObjectBuilder.getMessage();

	}

	@Test
	public void testMessageId() {

		mockMessage.setId(message.getId());
		Assert.assertEquals(message.getId(), mockMessage.getId());
	}

	@Test
	public void testGetText() {

		mockMessage.setText(message.getText());
		Assert.assertEquals(message.getText(), mockMessage.getText());
	}
}
