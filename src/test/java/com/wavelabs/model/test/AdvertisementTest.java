package com.wavelabs.model.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.builder.ObjectBuilder;
import com.wavelabs.model.Advertisement;
import com.wavelabs.model.enums.AdvertisementType;

@RunWith(MockitoJUnitRunner.class)
public class AdvertisementTest {

	@InjectMocks
	private Advertisement advertisementMock;

	private Advertisement advertisement;

	@Before
	public void setUp() {
		advertisement = ObjectBuilder.getAdvertisement();
	}

	@Test
	public void testGetId() {
		advertisementMock.setId(advertisement.getId());
		Assert.assertEquals(advertisement.getId(), advertisementMock.getId());
	}

	@Test
	public void testGetName() {
		advertisementMock.setName(advertisement.getName());
		Assert.assertEquals(advertisement.getName(), advertisementMock.getName());
	}

	@Test
	public void testGetType() {
		advertisementMock.setType(advertisement.getType());
		Assert.assertEquals(advertisement.getType(), advertisementMock.getType());
	}

	@Test
	public void testGetDescription() {
		advertisementMock.setDescription(advertisement.getDescription());
		Assert.assertEquals(advertisement.getDescription(), advertisementMock.getDescription());
	}

	@Test
	public void testGetLocation() {
		advertisementMock.setLocation(advertisement.getLocation());
		Assert.assertEquals(advertisement.getLocation(), advertisementMock.getLocation());
	}

	@Test
	public void testGetUser() {
		advertisementMock.setUser(advertisement.getUser());
		Assert.assertEquals(advertisement.getUser().getTenantId(), advertisementMock.getUser().getTenantId());
	}

	@Test
	public void testAdvertisementConstructor() {

		Advertisement add = new Advertisement(1, "Gopi krishna", AdvertisementType.BUSINESS, "Something",
				ObjectBuilder.getUser(), "HYD");
		Assert.assertEquals(1, add.getId());

	}
}
