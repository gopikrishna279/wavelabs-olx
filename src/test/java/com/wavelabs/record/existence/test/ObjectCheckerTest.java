package com.wavelabs.record.existence.test;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.builder.ObjectBuilder;
import com.wavelabs.record.existence.ObjectChecker;
import com.wavelabs.repo.UserRepo;

@RunWith(MockitoJUnitRunner.class)
public class ObjectCheckerTest {

	@Mock
	private UserRepo userRepo;

	@InjectMocks
	private ObjectChecker objectChecker;

	@Test
	public void testIsUserExist() {
		when(userRepo.findByUuidAndTenantId(anyString(), anyString())).thenReturn(ObjectBuilder.getUser());
		boolean flag = objectChecker.isUserExist("a", "b");
		Assert.assertEquals(true, flag);
	}
	@SuppressWarnings("unchecked")
	@Test
	public void testIsUserExist1() {
		when(userRepo.findByUuidAndTenantId(anyString(), anyString())).thenThrow(Exception.class);
		boolean flag = objectChecker.isUserExist("a", "b");
		Assert.assertEquals(false, flag);
	}

}
