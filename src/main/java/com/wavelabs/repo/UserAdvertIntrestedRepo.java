package com.wavelabs.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.wavelabs.model.User;
import com.wavelabs.model.UserAdvertIntrested;

public interface UserAdvertIntrestedRepo extends CrudRepository<UserAdvertIntrested, Integer> {

	
	public List<UserAdvertIntrested> findAllByUser(User user);
	
	
	
}
