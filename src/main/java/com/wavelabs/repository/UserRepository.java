package com.wavelabs.repository;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.model.User;
import com.wavelabs.repo.UserRepo;

@Component
public class UserRepository {

	private static final Logger logger = Logger.getLogger(UserRepository.class);

	@Autowired
	private UserRepo userRepo;

	/* this is to get usr */
	public User getUser(int id) {
		try {
			return userRepo.findOne(id);

		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	public boolean createUser(User user) {
		try {
			userRepo.save(user);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}
}
