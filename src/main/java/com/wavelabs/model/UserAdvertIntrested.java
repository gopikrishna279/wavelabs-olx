package com.wavelabs.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="user_interst")
public class UserAdvertIntrested {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@ManyToOne
	@JoinColumn(name= "user")
	private User user;
	@ManyToOne
	@JoinColumn(name= "advert")
	private Advertisement advert;

	public UserAdvertIntrested(int id, User user, Advertisement advert) {

		this.id = id;
		this.user = user;
		this.advert = advert;
	}

	public UserAdvertIntrested() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Advertisement getAdvert() {
		return advert;
	}

	public void setAdvert(Advertisement advert) {
		this.advert = advert;
	}
}
