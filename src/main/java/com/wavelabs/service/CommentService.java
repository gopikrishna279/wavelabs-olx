package com.wavelabs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.model.Comment;
import com.wavelabs.repository.CommentRepository;

@Service
public class CommentService {

	@Autowired
	private CommentRepository commentDao;

	public CommentService(CommentRepository commentDao) {
		this.commentDao = commentDao;
	}

	public boolean persistComment(Comment comment) {

		return commentDao.persistComment(comment);

	}

	public Comment[] getListOfCommentsOfAdvertisement(int id) {

		return commentDao.getAdvertisementComments(id);
	}

	public Comment updateComment(Comment comment) {

		return commentDao.updateComment(comment);
	}

	public Boolean deleteComment(int id) {
		return commentDao.deleteComment(id);
	}
	
}
