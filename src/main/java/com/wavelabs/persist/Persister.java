package com.wavelabs.persist;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.model.User;
import com.wavelabs.service.UserService;

/**
 * 
 * @author gopikrishnag
 */
@Component
public class Persister {

	@Autowired
	private UserService userService;

	public boolean persistUser(User user) {

		boolean flag = userService.createUser(user);
		return flag;
	}
}
